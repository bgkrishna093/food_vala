import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/components/CartItemCounter.dart';
import 'package:food_vala/model/FoodItemModel.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:food_vala/view/DashboardScreen.dart';

class PaymentScreen extends StatefulWidget {
  final String image;
  final int itemPrice;
  final int itemCount;
  final String name;

  const PaymentScreen(
      {super.key,
      required this.image,
      required this.itemPrice,
      required this.itemCount,
      required this.name});
      

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}


class _PaymentScreenState extends State<PaymentScreen> {
  int count = 0;
  int cutleryCount = 0;
  initState() {
    count = widget.itemCount;
  }
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: AppConstants.containerPadding,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30.h,),
              Text(
                "We will deliver in",
                style: Theme.of(context).textTheme.subtitle1?.copyWith(
                    color: AppConstants.labelColor,
                    fontWeight: FontWeight.w700),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "24 minutes to the address:",
                style: Theme.of(context).textTheme.subtitle1?.copyWith(
                    color: AppConstants.labelColor,
                    fontWeight: FontWeight.w700),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 20.h,),
              Text(
                "Hyderabad, Keus, Banjara Hills",
                style: Theme.of(context).textTheme.bodyText2?.copyWith(
                    color: AppConstants.labelColor,
                    fontWeight: FontWeight.w700),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 30.h,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset(
                    "assets/images/${widget.image}.png",
                    width: 100.w,
                  ),
                  //SizedBox(width: 20.h,),
                  SizedBox(
                    width: 120.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.name,
                          style: Theme.of(context).textTheme.bodyText2?.copyWith(
                              color: AppConstants.labelColor,
                              fontWeight: FontWeight.w700,
                              fontSize: 11.w),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 10.h,),
                        CartItemCounter(counter: count, onChanged: (int value) { 
                            count = value;
                            setState(() {
                              count = value;
                            });
                            print(value);
                         },)
                      ],
                    ),
                  ),
                  //SizedBox(width: 20.h,),
                  Text("\u{20B9} ${widget.itemPrice}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color:AppConstants.labelColor,fontWeight: FontWeight.w700, ),),
                ],
              ),
               SizedBox(height: 30.h,),
              Card(
                elevation: 11,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 20.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(Icons.restaurant,size: 30.w,),
                       Text("Cutlery",style: Theme.of(context).textTheme. bodyText2?.copyWith(color:AppConstants.labelColor,fontWeight: FontWeight.w700, ),),
                       CartItemCounter(counter: 1, onChanged: (int value) { 
                       
                        cutleryCount = value;
                      
                       },)
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30.h,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Delivery",style: Theme.of(context).textTheme. bodyText2?.copyWith(color:AppConstants.labelColor,fontWeight: FontWeight.w700, ),),
                      Text("Free delivery from \u{20B9} 200 ",style: Theme.of(context).textTheme. bodyText2?.copyWith(fontSize: 11.w,color:AppConstants.primaryColor,fontWeight: FontWeight.w700, ),),
                    ],
                  ),
                  Text("\u{20B9} 0.00",style: Theme.of(context).textTheme. bodyText2?.copyWith(color:AppConstants.labelColor,fontWeight: FontWeight.w700, ),),
                ],
              ),
               SizedBox(height: 30.h,),

              Text("Payment method",style: Theme.of(context).textTheme. bodyText2?.copyWith(color:AppConstants.primaryColor,fontWeight: FontWeight.w700, ),),
               SizedBox(height: 10.h,),
               Card(
                elevation: 11,
                child: Container(
                  padding: EdgeInsets.only(top: AppConstants.containerPadding,bottom: AppConstants.containerPadding,left: AppConstants.containerPadding,),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Google Pay",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700),),
                      IconButton(onPressed: (){}, icon: Icon(Icons.chevron_right_outlined,size: 20.w,)),
                    ],
                  ),
                ),
              ),
              Spacer(),
              ElevatedButton(style: ElevatedButton.styleFrom(primary:  AppConstants.labelColor,minimumSize:Size(150.w,40.h) ),
                onPressed: (){
                  setState(() {
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => DashboardScreen()),(route) => false);
                  });
                  }, 
                  child:Row(
                    mainAxisSize: MainAxisSize.max,
                    
                    children: [
                      Text('Pay',style: Theme.of(context).textTheme.bodyText2?.copyWith( fontSize: 11.sp, color: Colors.white,fontWeight: FontWeight.w700,),),
                      SizedBox(width: 200.w,),
                      Text('24 min',style: Theme.of(context).textTheme.bodyText2?.copyWith( fontSize: 11.sp, color: Colors.white,fontWeight: FontWeight.w700,),),
                      SizedBox(width: 10.w,),
                      Text("\u{20B9} ${count*widget.itemPrice}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: Colors.white,fontWeight: FontWeight.w700, ),),
                  ],)
                ),
                SizedBox(height: 20.h,),
            ],
          ),
        ),
      ),
    );
  }
}
