import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/components/AddCartItemCount.dart';
import 'package:food_vala/components/DetailWidget.dart';
import 'package:food_vala/model/FoodItemModel.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:food_vala/view/DashboardScreen.dart';

class AddToCartScreen extends StatefulWidget {
  final int index;
   AddToCartScreen({super.key, required this.index});
  @override
  State<AddToCartScreen> createState() => _AddToCartScreenState();
}
class _AddToCartScreenState extends State<AddToCartScreen> {
  int cartItemCount = 0;
  bool pay = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: AppConstants.containerPadding),
      child: Column(
        children: [
          SizedBox(height: 70.h,),
          Image.asset("assets/images/${foodItems[widget.index].image}.png",width: 200.w,),
          SizedBox(height: 30.h,),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(foodItems[widget.index].name,style: Theme.of(context).textTheme. subtitle1?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700),)),
          SizedBox(height: 20.h,),
          Text(foodItems[widget.index].dis,style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.primaryColor,fontWeight: FontWeight.w700),),
          SizedBox(height: 30.h,),
          Container(
            padding: EdgeInsets.symmetric(vertical: AppConstants.containerPadding),
            decoration:  BoxDecoration(
              border: Border.all(width: 2.0.w, color: Colors.black54),
              borderRadius: BorderRadius.all( Radius.circular(AppConstants.containerPadding) ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                DetailWidget(index: widget.index, name: "Kcal"),
                DetailWidget(index: widget.index, name: "grams"),
                DetailWidget(index: widget.index, name: "protein"),
                DetailWidget(index: widget.index, name: "fats"),
                DetailWidget(index: widget.index, name: "carbs")
              ],
            ),
          ),
        SizedBox(height: 30.h,),
        Card(
          elevation: 11,
          child: Container(
            padding: EdgeInsets.only(top: AppConstants.containerPadding,bottom: AppConstants.containerPadding,left: AppConstants.containerPadding,),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                 Text("Add in poke",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700),),
                 IconButton(onPressed: (){}, icon: Icon(Icons.chevron_right_outlined,size: 20.w,))
              ],
            ),
          ),
        ),
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AddCartItemCount(minValue: 0, maxValue: 10, onChanged: (value){
          setState(() {
            cartItemCount = value;
          });
        }),
        ElevatedButton(
           style: ElevatedButton.styleFrom(primary:  AppConstants.labelColor,minimumSize:Size(150.w,40.h), ),
          onPressed: (){
            setState(() {
              if(cartItemCount != 0){
                pay = true;
              }
            });
            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => DashboardScreen(paymentButtonStatu: pay, itemPrice:foodItems[widget.index].price, itemCount:cartItemCount, itemName: foodItems[widget.index].name,image:foodItems[widget.index].image ,),),(route) => false);
          }, child:  Text("Add to cart     \u{20B9}${cartItemCount*foodItems[widget.index].price}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: Colors.white,fontWeight: FontWeight.w700),), )
          ],
        ),
        SizedBox(height: 20.h,)
        ],
      ),
    );
  }
}