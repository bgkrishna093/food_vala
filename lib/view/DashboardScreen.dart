import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/components/FoodItemUI.dart';
import 'package:food_vala/components/FormInputField.dart';
import 'package:food_vala/components/HitsFoodItem.dart';
import 'package:food_vala/model/FoodItemModel.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:food_vala/view/PaymentScreen.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class DashboardScreen extends StatefulWidget {
  //DashboardScreen({Key? key}) : super(key: key);
 String itemName;
 bool paymentButtonStatu;
 int itemCount;
 int itemPrice;
 String image;

 DashboardScreen({this.itemName = "" ,this.paymentButtonStatu = false,  this.itemCount = 0,this.itemPrice = 0, this.image = ""});
  
  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}
class _DashboardScreenState extends State<DashboardScreen> {
  TextEditingController locNameController = TextEditingController();
  FocusNode locNameFocus = FocusNode();
  late PageController  controller;
   int currPageIndex = 0;
   PageController getPageController(){
    controller = PageController( initialPage: currPageIndex);
    return controller;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
           SizedBox(
            height: 100.h,
            child: Stack(
              children: [
                Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child:  SizedBox(
                      width: 0.65.sw,
                      child: FormInputField(
                      keyboardType: TextInputType.streetAddress,
                      controller: locNameController,
                      focusNode: locNameFocus,
                      hintText: "Search your location",
                      maxLength: 64,
                      validator: null,
                      onChanged: (value) => null, 
                    ),
                  )
                ),
                Positioned.fill(
                  left: 0,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Builder(
                      builder: (ctx){
                        return IconButton(
                            onPressed: (){
                             
                            },
                            icon: Icon(Icons.menu_sharp,color: AppConstants.labelColor,size: 32.w,)
                        );
                      },
                    ),
                  )
                ),
                Positioned.fill(
                    right: 0,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                          onPressed: (){            
                          },
                          icon:  Icon(Icons.search,color: AppConstants.labelColor,size: 32.w,),
                      ),
                    )
                )
              ],
            ),
          ),        
          Padding(
            padding: EdgeInsets.symmetric(horizontal: AppConstants.containerPadding),
            child: Text("Hits of the week",style: Theme.of(context).textTheme. subtitle1?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700),),
          ),
          SizedBox(height: 10.h,),
          Padding(
            padding:  EdgeInsets.symmetric(horizontal: AppConstants.containerPadding),
            child: SizedBox(
              height: 250.h,
              child:  PageView(
                controller: getPageController(),
                onPageChanged: (int page){
                  setState(() {
                    currPageIndex = page;
                  });
                },
                children: List.generate(foodItems.length, (index){
                  return  HitsFoodItem(name: foodItems[index].name, image: foodItems[index].image, energy: foodItems[index].energy, price: foodItems[index].price, index: currPageIndex ,);
                  
                }),
              )
            ),
          ),
          Center(
            child: SmoothPageIndicator(  
                controller: controller,  // PageController  
                count: foodItems.length,  
                effect: SlideEffect(  
                    spacing:  20.0.w,  
                    radius:  2.h,  
                    dotWidth:  50.w,  
                    dotHeight:  4.w,  
                    paintStyle:  PaintingStyle.fill,  
                    strokeWidth:  1.5,  
                    dotColor:  Colors.grey.shade300,  
                    activeDotColor:  Colors.indigo,                       
                  ),  // your preferred effect  
                onDotClicked: (index){  
                  setState(() {
                      currPageIndex = index;
                      controller.jumpToPage(index);
                    });
                }  
              ),
          ),
          SizedBox(height: 10.h,),
          SizedBox(
          height: 25.h,
          width: double.infinity,
          child:  ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: AppConstants.categories.length,
            itemBuilder: (BuildContext context, int index) {
              return  InkWell(
                onTap: (){},
                child: Container(
                  margin:EdgeInsets.symmetric(horizontal: 10.w,) ,
                    padding: EdgeInsets.symmetric(horizontal:AppConstants.containerPadding , vertical: 5.w),
                    decoration: BoxDecoration(
                      color: AppConstants.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(AppConstants.containerRadius))
                    ),
                    child: Text(AppConstants.categories[index],style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,)),
              );
            }),
          ),
          Expanded(
            child: 
            Stack(
              children: [
                ListView.builder(
                  itemCount: foodItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    return FoodItemUI(index: index);
                  }),
                Positioned.fill(
                  bottom: 50.h,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child:Visibility(
                          visible: widget.paymentButtonStatu,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: AppConstants.containerPadding),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(primary:  AppConstants.labelColor,minimumSize:Size(150.w,40.h) ),
                            onPressed: (){
                              setState(() {
                                widget.paymentButtonStatu = false;
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => PaymentScreen(image: widget.image, itemPrice: widget.itemPrice, itemCount: widget.itemCount, name: widget.itemName)),(route) => false);
                              });
                              }, 
                              child:Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Text('Pay',style: Theme.of(context).textTheme.bodyText2?.copyWith( fontSize: 11.sp, color: Colors.white,fontWeight: FontWeight.w700,),),
                                  SizedBox(width: 200.w,),
                                  Text('24 min',style: Theme.of(context).textTheme.bodyText2?.copyWith( fontSize: 11.sp, color: Colors.white,fontWeight: FontWeight.w700,),),
                                  SizedBox(width: 10.w,),
                                  Text("\u{20B9} ${widget.itemCount*widget.itemPrice}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: Colors.white,fontWeight: FontWeight.w700, ),),
                              ],)
                             ),
                          ),
                        )
                      )
                  )

              ],
            )
          ),  
        ],
      ),

    );
    
  }
}