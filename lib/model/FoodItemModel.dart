

class FoodItem {
  final String name;
  final String image;
  final int price;
  final String energy;
  final String dis;
  final Map<String, dynamic> details;

  FoodItem( {required this.name, required this.image, required this.price, required this.energy, required this.dis,required this.details,});

factory FoodItem.fromMap(Map<String, dynamic> json) => FoodItem(
  energy: json["energy"], 
  image: json["image"], 
  name: json["name"], 
  price: json["price"], 
  dis: json["dis"], 
  details:json["details"]
);
  
} 

List<FoodItem> foodItems = [
  FoodItem(name: "Masala Chicken Lollipop", image: "chickenLoli", price: 220, energy: "300 Kcal", dis: 'Chicken lollipop is an appetiser popular in Indian cuisine. Chicken lollipop is, essentially a frenched chicken winglet, wherein the meat is cut loose from the bone end and pushed down creating a lollipop appearance.', details: {
    "Kcal":300,
    "grams":150,
    "protein":200,
    "fats":20,
    "carbs":150
  }),
  FoodItem(name: "Seven slices of pizza with delicious salami", image: "pizza", price: 200, energy: "150 Kcal", dis: 'pizza, dish of Italian origin consisting of a flattened disk of bread dough topped with some combination of olive oil, oregano, tomato, olives, mozzarella or other cheese, and many other ingredients, baked quickly—usually, in a commercial setting, using a wood-fired oven heated to a very high temperature—and served hot.',details: {
    "Kcal":200,
    "grams":200,
    "protein":200,
    "fats":20,
    "carbs":200
  }),
  FoodItem(name: "Hyderabad Biryani", image: "biryani", price: 250, energy: "300 Kcal", dis: 'Simply put, biryani is a spiced mix of meat and rice, traditionally cooked over an open fire in a leather pot. It is combined in different ways with a variety of components to create a number of highly tasty and unique flavor combinations. ',details: {
    "Kcal":300,
    "grams":500,
    "protein":250,
    "fats":40,
    "carbs":1000
  }),
   FoodItem(name: "Masala Chicken Lollipop", image: "chickenLoli", price: 220, energy: "300 Kcal", dis: 'Chicken lollipop is an appetiser popular in Indian cuisine. Chicken lollipop is, essentially a frenched chicken winglet, wherein the meat is cut loose from the bone end and pushed down creating a lollipop appearance.', details: {
    "Kcal":300,
    "grams":150,
    "protein":200,
    "fats":20,
    "carbs":150
  }),
  FoodItem(name: "Seven slices of pizza with delicious salami", image: "pizza", price: 200, energy: "150 Kcal", dis: 'pizza, dish of Italian origin consisting of a flattened disk of bread dough topped with some combination of olive oil, oregano, tomato, olives, mozzarella or other cheese, and many other ingredients, baked quickly—usually, in a commercial setting, using a wood-fired oven heated to a very high temperature—and served hot.',details: {
    "Kcal":200,
    "grams":200,
    "protein":200,
    "fats":20,
    "carbs":200
  }),
  FoodItem(name: "Hyderabad Biryani", image: "biryani", price: 250, energy: "300 Kcal", dis: 'Simply put, biryani is a spiced mix of meat and rice, traditionally cooked over an open fire in a leather pot. It is combined in different ways with a variety of components to create a number of highly tasty and unique flavor combinations. ',details: {
    "Kcal":300,
    "grams":500,
    "protein":250,
    "fats":40,
    "carbs":1000
  }),
    FoodItem(name: "Masala Chicken Lollipop", image: "chickenLoli", price: 220, energy: "300 Kcal", dis: 'Chicken lollipop is an appetiser popular in Indian cuisine. Chicken lollipop is, essentially a frenched chicken winglet, wherein the meat is cut loose from the bone end and pushed down creating a lollipop appearance.', details: {
    "Kcal":300,
    "grams":150,
    "protein":200,
    "fats":20,
    "carbs":150
  }),
  FoodItem(name: "Seven slices of pizza with delicious salami", image: "pizza", price: 200, energy: "150 Kcal", dis: 'pizza, dish of Italian origin consisting of a flattened disk of bread dough topped with some combination of olive oil, oregano, tomato, olives, mozzarella or other cheese, and many other ingredients, baked quickly—usually, in a commercial setting, using a wood-fired oven heated to a very high temperature—and served hot.',details: {
    "Kcal":200,
    "grams":200,
    "protein":200,
    "fats":20,
    "carbs":200
  }),
  FoodItem(name: "Hyderabad Biryani", image: "biryani", price: 250, energy: "300 Kcal", dis: 'Simply put, biryani is a spiced mix of meat and rice, traditionally cooked over an open fire in a leather pot. It is combined in different ways with a variety of components to create a number of highly tasty and unique flavor combinations. ',details: {
    "Kcal":300,
    "grams":500,
    "protein":250,
    "fats":40,
    "carbs":1000
  }),

];