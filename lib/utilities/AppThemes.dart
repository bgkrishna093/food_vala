import 'package:flutter/material.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';



class AppThemes{
  AppThemes._();

  static ThemeData get lightTheme{
    return ThemeData(
        primaryColor: AppConstants.primaryColor,
        scaffoldBackgroundColor: AppConstants.scaffoldBg,
        appBarTheme: const AppBarTheme(
            backgroundColor: AppConstants.primaryColor,
            elevation: 2.0
        ),
        bottomAppBarTheme: const BottomAppBarTheme(

        ),
        fontFamily: GoogleFonts.openSans().fontFamily,
        textTheme: TextTheme(
            headline1: TextStyle(
                fontSize: 50.sp,
                fontWeight: FontWeight.bold
            ),
            headline2: TextStyle(
                fontSize: 45.sp,
                fontWeight: FontWeight.bold
            ),
            headline3: TextStyle(
                fontSize: 40.sp,
                fontWeight: FontWeight.bold
            ),
            headline4: TextStyle(
                fontSize: 35.sp,
                fontWeight: FontWeight.bold
            ),
            headline5: TextStyle(
                fontSize: 30.sp,
                fontWeight: FontWeight.bold
            ),
            headline6: TextStyle(
                fontSize: 20.sp,
                fontWeight: FontWeight.bold
            ),
            subtitle1: TextStyle(
                fontSize: 17.sp
            ),
            subtitle2: TextStyle(
                fontSize: 13.sp
            ),
            bodyText1: TextStyle(
                fontSize: 14.sp,
                color: AppConstants.labelColor
            ),
            bodyText2: TextStyle(
                fontSize: 12.sp,
                color: AppConstants.labelColor
            ),
            caption: const TextStyle()
        ),
        inputDecorationTheme: InputDecorationTheme(
            labelStyle: TextStyle(
                fontSize: 15.sp
            ),
            hintStyle: TextStyle(
                fontSize: 13.sp
            ),
            errorStyle: TextStyle(
                fontSize: 13.sp
            ),
            contentPadding: EdgeInsets.only(top: AppConstants.inputPadding, bottom: AppConstants.inputPadding,right: AppConstants.inputPadding,left: AppConstants.inputPaddingLeft),
            fillColor: Colors.white,
            filled: true,
            border: OutlineInputBorder(
                //borderSide: const BorderSide(color: Colors.white,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(AppConstants.inputRadius))
            ),
            enabledBorder: OutlineInputBorder(
                //borderSide: const BorderSide(color: Colors.white,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(AppConstants.inputRadius))
            ),
            disabledBorder: OutlineInputBorder(
                //borderSide: const BorderSide(color: Colors.white,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(AppConstants.inputRadius))
            ),
            focusedBorder: OutlineInputBorder(
                //borderSide: const BorderSide(color: Colors.white,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(AppConstants.inputRadius))
            ),
            errorBorder: OutlineInputBorder(
                //borderSide: const BorderSide(color: Colors.red,width: 1),
                borderRadius: BorderRadius.all(Radius.circular(AppConstants.inputRadius))
            )
        ),
        tabBarTheme: const TabBarTheme(

        ),
        tooltipTheme: const TooltipThemeData(

        ),
        snackBarTheme: const SnackBarThemeData(
          backgroundColor: AppConstants.labelColor,
          actionTextColor: Colors.white
        ),
        chipTheme: ChipThemeData(
            backgroundColor: AppConstants.primaryColor,
            disabledColor: AppConstants.disabledColor,
            shape: const StadiumBorder(),
            brightness: Brightness.light,
            labelPadding: EdgeInsets.all(AppConstants.chipPadding),
            labelStyle: const TextStyle(),
            padding: EdgeInsets.all(AppConstants.chipPadding),
            secondaryLabelStyle: const TextStyle(),
            secondarySelectedColor: Colors.white38,
            selectedColor: Colors.white
        ),
        dialogTheme: const DialogTheme(

        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(

        ),
        bottomSheetTheme: const BottomSheetThemeData(

        ),
        popupMenuTheme: const PopupMenuThemeData(

        ),
        dividerTheme: const DividerThemeData(

        ),
        buttonTheme: ButtonThemeData(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppConstants.buttonBorderRadius)
            )
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              primary: const Color(0xffFFA000),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(AppConstants.buttonBorderRadius)
              )
          ),
        ),
        textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              primary: AppConstants.primaryColor,
            )
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
            style: OutlinedButton.styleFrom(
                primary: AppConstants.primaryColor
            )
        ),
        cardTheme: ThemeData.light().cardTheme.copyWith(
            color: Colors.white,
            margin: EdgeInsets.all(AppConstants.cardMargin),
            elevation: AppConstants.cardElevation,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(AppConstants.cardRadius)
            )
        ),
        checkboxTheme: CheckboxThemeData(
          checkColor: MaterialStateColor.resolveWith((states){
            if(states.contains(MaterialState.selected)){
              return Colors.white;
            }else{
              return Colors.white;
            }
          }),
          fillColor: MaterialStateColor.resolveWith((states){
            if(states.contains(MaterialState.selected)){
              return AppConstants.labelColor;
            }else{
              return Colors.white;
            }
          }),
        ),
        radioTheme: RadioThemeData(
            fillColor: MaterialStateColor.resolveWith((states){
              if(states.contains(MaterialState.selected)){
                return AppConstants.primaryColor;
              }else{
                return Colors.grey;
              }
            })
        ),
        switchTheme: SwitchThemeData(
            thumbColor: MaterialStateColor.resolveWith((states){
              if(states.contains(MaterialState.selected)){
                return AppConstants.primaryColor;
              }else{
                return AppConstants.disabledColor;
              }
            }),
            trackColor: MaterialStateColor.resolveWith((states){
              if(states.contains(MaterialState.selected)){
                return AppConstants.primaryColorLight;
              }else{
                return Colors.grey.shade200;
              }
            })
        ),
        sliderTheme: SliderThemeData(
            thumbColor: AppConstants.primaryColor,
            disabledThumbColor: AppConstants.disabledColor,
            activeTrackColor: AppConstants.primaryColor,
            inactiveTrackColor: AppConstants.primaryColorLight
        ),
        
      scrollbarTheme: ScrollbarThemeData(
        thumbColor: MaterialStateProperty.all(const Color(0xff5D5D5D)),
        trackColor: MaterialStateProperty.all(const Color(0xffC4C4C4)),
      )
    );
  }

}