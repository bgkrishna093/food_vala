import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/utilities/AppConstants.dart';

class AddCartItemCount extends StatefulWidget {
  final int minValue;
  final int maxValue;
  final ValueChanged<int> onChanged;
  const AddCartItemCount(
      {super.key,
      required this.minValue,
      required this.maxValue,
      required this.onChanged});
  @override
  State<AddCartItemCount> createState() {
    return _AddCartItemCount();
  }
}

class _AddCartItemCount extends State<AddCartItemCount> {
  int counter = 0;
  @override
  Widget build(BuildContext context) { 
    return Container(
      decoration: BoxDecoration(
          color: AppConstants.primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10.w))),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            icon: Icon(
              Icons.remove,
              color: AppConstants.labelColor,
            ),
            iconSize: 15.0.h,
            onPressed: () {
              setState(() {
                if (counter > widget.minValue) {
                  counter--;
                }
                widget.onChanged(counter);
              });
            },
          ),
          SizedBox(
            width: 10.w,
          ),
          Text(
            "$counter",
            style: Theme.of(context).textTheme.bodyText2?.copyWith(
                color: AppConstants.labelColor, fontWeight: FontWeight.w700),
          ),
          SizedBox(
            width: 10.w,
          ),
          IconButton(
            icon: Icon(
              Icons.add,
              color: AppConstants.labelColor,
            ),
            iconSize: 15.0.h,
            onPressed: () {
              setState(() {
                if (counter < widget.maxValue) {
                  counter++;
                }
                widget.onChanged(counter);
              });
            },
          ),
        ],
      ),
    );
  }
}
