import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:food_vala/view/AddToCartScreen.dart';


class HitsFoodItem extends StatelessWidget {
  final String name;
  final String image;
  final String energy;
  final int price;
  final index;

  const HitsFoodItem({super.key, required this.name, required this.image, required this.energy, required this.price, required this.index});


  @override
  Widget build(BuildContext context) {
    return InkWell(
       onTap: (){
         showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return AddToCartScreen(index: index,);
          });
      },
      child: Stack(
        children: [
          SizedBox(
            height: 250.h,
            //width: double.infinity,
            child: Center(
              child: Container(
                padding: EdgeInsets.only(right: AppConstants.containerPadding, left: AppConstants.containerPadding, top: 120.h),
                height: 200.h,
               // width: 0.9.sw,
                decoration: BoxDecoration(
                  gradient: LinearGradient(begin: Alignment.topLeft,end: Alignment.bottomRight,colors: [Colors.purple,Colors.purple.shade200 ]),
                  borderRadius: BorderRadius.all(Radius.circular(AppConstants.containerRadius))),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 0.4.sw,
                        child: Text(name,style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal:AppConstants.containerPadding20 , vertical: AppConstants.containerPadding),
                          decoration: BoxDecoration(
                            color: AppConstants.labelColor,
                            borderRadius: BorderRadius.all(Radius.circular(30.w))
                          ),
                          child: Text("\u{20B9}$price",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: Colors.white,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,)),
                    ],
                  ),                           
              ),
            ),
          ),
          Positioned.fill(
              top: 0,
              child: Align(
                alignment: Alignment.topCenter,
                child: Image.asset("assets/images/$image.png",width: 180.w,)
              )
          )
        ],
      ),
    );
    
  }
}