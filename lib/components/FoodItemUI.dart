import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/model/FoodItemModel.dart';
import 'package:food_vala/utilities/AppConstants.dart';
import 'package:food_vala/view/AddToCartScreen.dart';


class FoodItemUI extends StatelessWidget {
final int index;
  const FoodItemUI({super.key, required this.index});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
         showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return 
              
              AddToCartScreen(index: index,);
          });
      },
      child: Padding(
        padding:  EdgeInsets.symmetric(horizontal: AppConstants.containerPadding),
        child: Card(
          elevation: 11,
          child: Padding(
            padding:  EdgeInsets.all(8.w),
            child: Row(
              children: [
                Image.asset("assets/images/${foodItems[index].image}.png",width: 100.w,),
                SizedBox(width: 30.w,),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 150.w,
                      child: Text(foodItems[index].name,style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,)),
                    SizedBox(height: 10.h,),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        
                        Container(
                          padding: EdgeInsets.symmetric(horizontal:AppConstants.containerPadding20 , vertical: AppConstants.containerPadding),
                          decoration: BoxDecoration(
                            color: AppConstants.primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(30.w))
                          ),
                          child: Text("\u{20B9}${foodItems[index].price}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.labelColor,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,)),
                        SizedBox(width: 20.w,),
                        Text(foodItems[index].energy,style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.primaryColor,fontWeight: FontWeight.w700, ),maxLines: 2, overflow: TextOverflow.ellipsis,),
                      ],
                    ) 
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );  
  }
}