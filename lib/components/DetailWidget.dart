import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:food_vala/model/FoodItemModel.dart';
import 'package:food_vala/utilities/AppConstants.dart';

class DetailWidget extends StatelessWidget {
  final int index;
  final String name;

  const DetailWidget({super.key, required this.index, required this.name});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text("${foodItems[index].details[name]}",style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.primaryColor,fontWeight: FontWeight.w700),),
        Text(name,style: Theme.of(context).textTheme. bodyText2?.copyWith(color: AppConstants.primaryColor,fontWeight: FontWeight.w700),),
      ],
    );
  }
}