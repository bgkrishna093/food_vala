
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/utilities/AppConstants.dart';


class FormInputField extends StatelessWidget {
  const FormInputField(
      {
        Key? key,
        required this.controller,
        required this.focusNode,
        required this.hintText,
        required this.validator,
        this.keyboardType = TextInputType.text,
        this.obscureText = false,
        this.minLines = 1,
        required this.maxLength,
        required this.onChanged,
        this.isEnabled = true
      }) : super(key: key);

  final int maxLength;
  final TextEditingController controller;
  final FocusNode focusNode;
  final String hintText;
  final String? Function(String?)? validator;
  final TextInputType keyboardType;
  final bool obscureText;
  final int minLines;
  final void Function(String) onChanged;
  final bool isEnabled;


  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: isEnabled,
      inputFormatters: [
        LengthLimitingTextInputFormatter(maxLength)
      ],
    
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle:TextStyle(
        fontSize: 13.sp,
        color: Colors.white70
      ) ,
        filled: true,
        fillColor: isEnabled ? AppConstants.labelColor : AppConstants.disabledColor
      ),
      
      style: TextStyle(
        fontSize: 13.sp,
        color: Colors.white
      ),
      controller: controller,
      focusNode: focusNode,
      onChanged: onChanged,
      keyboardType: keyboardType,
      obscureText: obscureText,
      maxLines: 1,
      minLines: minLines,
      validator: validator,
    );
  }
}