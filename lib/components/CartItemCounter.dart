

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:food_vala/utilities/AppConstants.dart';

class CartItemCounter extends StatefulWidget {
  //const CartItemCounter({Key? key}) : super(key: key);
int counter;
 ValueChanged<int> onChanged;
CartItemCounter({super.key, this.counter=0, required this.onChanged});

  @override
  State<CartItemCounter> createState() => _CartItemCounterState();
}

class _CartItemCounterState extends State<CartItemCounter> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
            color: AppConstants.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10.w))),
            child: IconButton(
              padding: EdgeInsets.all(10.w),
              constraints: BoxConstraints(),
              icon: Icon(
                Icons.remove,
                color: AppConstants.labelColor,
              ),
              iconSize: 10.0.h,
              onPressed: () {
                setState(() {
                  if (widget.counter > 0) {
                    widget.counter--;
                  }
                  widget.onChanged(widget.counter);
                });
              },
            ),
          ),
          SizedBox(
            width: 10.w,
          ),
          Text(
            "${widget.counter}",
            style: Theme.of(context).textTheme.bodyText2?.copyWith(
                color: AppConstants.labelColor, fontWeight: FontWeight.w700),
          ),
          SizedBox(
            width: 10.w,
          ),
          Container(
            decoration: BoxDecoration(
            color: AppConstants.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10.w))),
            child: IconButton(
              padding: EdgeInsets.all(10.w),
              constraints: BoxConstraints(),
              icon: Icon(
                Icons.add,
                color: AppConstants.labelColor,
              ),
              iconSize: 10.0.h,
              onPressed: () {
                setState(() {
                  if (widget.counter < 10) {
                    widget.counter++;
                    print(widget.counter);
                  }
                  widget.onChanged(widget.counter);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}